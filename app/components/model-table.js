import Component from '@ember/component';
import Table from 'ember-light-table';

export default Component.extend({
   classNames: ['model-table'],
   model: null,
   table: null,
   columns: null,

  init() {
    this._super(...arguments);

    let table = new Table(this.get('columns'), this.get('model'), { enableSync: this.get('enableSync') });
    this.set('table', table);
  },
});
