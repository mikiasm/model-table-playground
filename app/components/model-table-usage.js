import Component from '@ember/component';

export default Component.extend({
  classNames: ['model-table-usage'],

  init(){
    this._super(...arguments);

    this.set('columns', [{
        label: '',
        valuePath: 'check',
        cellComponent: 'model-table-checkbox',
        component:  'model-table-checkbox',
      }, {
        label: 'Name',
        valuePath: 'name',
      }, {
        label: 'Status',
        valuePath: 'status',
      },{
        label: 'Last Modified',
        valuePath: 'lastModified'
      },{
        label: 'Date Created',
        valuePath: 'dateCreated',
      },{
        label: '',
        valuePath: 'actions',
        cellComponent: 'model-table-actions-dropdown',
      }
    ]);

    this.set('model', [
      {
        name: 'My Awesome Campaign 1.0',
        status: 'Active',
        lastModified: 'Nov 27/2018',
        dateCreated: 'Nov 27/2018',
        actions: '...',
        bio: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      },
      {
        name: 'My Other Awesome Campaign 2.0',
        status: 'Active',
        lastModified: 'Nov 27/2018',
        dateCreated: 'Nov 27/2018',
        actions: '...',
        bio: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      },
      {
        name: 'My Other Awesome Campaign 2.0',
        status: 'Active',
        lastModified: 'Nov 27/2018',
        dateCreated: 'Nov 27/2018',
        actions: '...',
        bio: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      },
      {
        name: 'My Other Awesome Campaign 2.0',
        status: 'Active',
        lastModified: 'Nov 27/2018',
        dateCreated: 'Nov 27/2018',
        actions: '...',
        bio: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      },
      {
        name: 'My Other Awesome Campaign 2.0',
        status: 'Active',
        lastModified: 'Nov 27/2018',
        dateCreated: 'Nov 27/2018',
        actions: '...',
        bio: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      },
      {
        name: 'My Other Awesome Campaign 2.0',
        status: 'Active',
        lastModified: 'Nov 27/2018',
        dateCreated: 'Nov 27/2018',
        actions: '...',
        bio: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      }
    ]);
  }
});
