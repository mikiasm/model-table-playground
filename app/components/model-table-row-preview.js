import EmberObject from "@ember/object";
import Component from '@ember/component';

export default Component.extend({
  classNames: ['model-table-row-preview'],

  init() {
      this._super(...arguments);

      this.set('btns', EmberObject.create({
        isStrategy: false,
        isExperience: false,
        isAudience: false,
        isPush: false,
        isInApp: false,
      }))
  },

  resetConditions(item) {
    for (let key in this.btns) {
      if (this.btns.hasOwnProperty(key)) {
        if (key == item) {
          this.btns.set(key, true);
        } else {
          this.btns.set(key, false);
        }
      }
    }
  },

  actions: {
    expand(item) {

      switch (item) {
        case 1:
            this.set('btns.isStrategy', true);
            this.resetConditions('isStrategy');
          break;
        case 2:
            this.set('btns.isExperience', true);
            this.resetConditions('isExperience');
          break;
        case 3:
            this.set('btns.isAudience', true);
            this.resetConditions('isAudience');
          break;
        case 4:
            this.set('btns.isPush', true);
            this.resetConditions('isPush');
          break;
        case 5:
            this.set('btns.isInApp', true);
            this.resetConditions('isInApp');
          break;
        default:
          this.set('btns.isStrategy', true);
      }
    },
  }
});
