import Component from '@ember/component';

export default Component.extend({
  actions: {
    click(event) {
      // NOTE so it does not trigger the click event on the row (which also
      //      manage row selection)
      event.stopPropagation();
    }
  }
});
