import Component from '@ember/component';

export default Component.extend({
  classNames: ['model-table-actions-dropdown'],

  actions: {
    test(e) {
      e.preventDefault();
    }
  }
});
